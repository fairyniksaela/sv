(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./main.js":
/*!*****************!*\
  !*** ./main.js ***!
  \*****************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! @styles/main.scss */ "./styles/main.scss");

var _app = _interopRequireDefault(__webpack_require__(/*! @main/app */ "./scripts/main/app.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//import "@styles/main.less";
window.App = new _app["default"](); //Переход по кнопке на страницу

var btn = document.getElementById("login-btn");
var inp = document.querySelector(".input-login");

var getPage = function getPage() {
  btn.addEventListener("click", function () {
    if (inp.value != "") {
      window.open("/brands.html");
    } else {
      alert("Введите данные!");
    }
  });
};

getPage();

/***/ }),

/***/ "./scripts/main/app.js":
/*!*****************************!*\
  !*** ./scripts/main/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "../node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "../node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "../node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.from */ "../node_modules/core-js/modules/es.array.from.js");

__webpack_require__(/*! core-js/modules/es.array.is-array */ "../node_modules/core-js/modules/es.array.is-array.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "../node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.slice */ "../node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.date.to-string */ "../node_modules/core-js/modules/es.date.to-string.js");

__webpack_require__(/*! core-js/modules/es.function.name */ "../node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "../node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.regexp.to-string */ "../node_modules/core-js/modules/es.regexp.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "../node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "../node_modules/core-js/modules/web.dom-collections.iterator.js");

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//Вертикальное меню
var hideMenu = document.getElementById("hide");
var hideContainer = document.getElementById("header-container");
var changeContainer = document.querySelector(".change-container");
var activeText = hideContainer.querySelectorAll(".active-text");
var hideText = hideContainer.querySelectorAll(".disable-text");
var smallContainer = document.querySelector(".small");
var optionContainer = document.querySelector(".option-wrappers-items");
var bigPad = document.querySelector(".pad");
var optionNavigate = document.querySelector(".options-navigate");
var backChange = document.querySelector(".back-change");
var backButton = document.querySelector(".back");
var hideSvg = document.querySelector(".hide-svg");
var svg = document.querySelector(".items-svg");
var rightIcon = document.querySelector(".header-logo-right");
var icon = document.querySelectorAll(".header-logo");
var hideContent = document.querySelector(".menu-category");
var blockContent = document.querySelector(".hide-block");
var leftFrame = document.querySelector(".check-containers-left");
var frame = document.querySelector(".check-containers");

var getLeftFrame = function getLeftFrame() {
  var changeFrame = document.querySelectorAll(".check-containers");

  var _iterator = _createForOfIteratorHelper(changeFrame),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      frame = _step.value;
      if (frame.classList.toggle("check-containers-left")) ;
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
};

var getRightIcon = function getRightIcon() {
  var changeIcon = document.querySelectorAll(".header-logo");

  var _iterator2 = _createForOfIteratorHelper(changeIcon),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      icon = _step2.value;
      if (icon.classList.toggle("header-logo-right")) ;
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }
};

var getBigPad = function getBigPad() {
  var changePad = document.querySelectorAll(".options-navigate");

  var _iterator3 = _createForOfIteratorHelper(changePad),
      _step3;

  try {
    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
      optionNavigate = _step3.value;
      if (optionNavigate.classList.toggle("pad")) ;
    }
  } catch (err) {
    _iterator3.e(err);
  } finally {
    _iterator3.f();
  }
};

var getSmallContainer = function getSmallContainer() {
  var changeContainer = document.querySelectorAll(".option-wrappers-items");

  var _iterator4 = _createForOfIteratorHelper(changeContainer),
      _step4;

  try {
    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      optionContainer = _step4.value;
      if (optionContainer.classList.toggle("small")) ;
    }
  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }
};

var getSmallMenu = function getSmallMenu() {
  var change = document.querySelectorAll(".header-container");

  var _iterator5 = _createForOfIteratorHelper(change),
      _step5;

  try {
    for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
      hideContainer = _step5.value;
      if (hideContainer.classList.toggle("change-container")) ;
    }
  } catch (err) {
    _iterator5.e(err);
  } finally {
    _iterator5.f();
  }
};

var getChangeBack = function getChangeBack() {
  var changeButton = document.querySelectorAll(".back");

  var _iterator6 = _createForOfIteratorHelper(changeButton),
      _step6;

  try {
    for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
      backButton = _step6.value;
      if (backButton.classList.toggle("back-change")) ;
    }
  } catch (err) {
    _iterator6.e(err);
  } finally {
    _iterator6.f();
  }
};

var getHideSvg = function getHideSvg() {
  var changeSvg = document.querySelectorAll(".items-svg");

  var _iterator7 = _createForOfIteratorHelper(changeSvg),
      _step7;

  try {
    for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
      svg = _step7.value;
      if (svg.classList.toggle("hide-svg")) ;
    }
  } catch (err) {
    _iterator7.e(err);
  } finally {
    _iterator7.f();
  }
};

hideMenu.addEventListener("click", function () {
  var all = document.querySelectorAll(".active-text");

  var _iterator8 = _createForOfIteratorHelper(all),
      _step8;

  try {
    for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
      activeText = _step8.value;
      if (activeText.classList.toggle("disable-text")) ;
    }
  } catch (err) {
    _iterator8.e(err);
  } finally {
    _iterator8.f();
  }

  getSmallMenu();
  getRightIcon();
  getSmallContainer();
  getBigPad();
  getChangeBack();
  getHideSvg();
  getLeftFrame();
}); //Меню на мобильных устройствах

var btnMenu = document.querySelector(".header-menu-brand");
var openMenu = document.querySelector(".btn-menu");
var openBlock = document.querySelectorAll(".block-menu");
var elementBlock = document.querySelector(".menu-category");

var getMenu = function getMenu() {
  openMenu.addEventListener("click", function () {
    if (hideContainer.classList.toggle("block-menu")) {
      elementBlock.classList.toggle("hide-block");
    } else {
      elementBlock.classList.add("hide-block");
    }
  });
};

getMenu();

/***/ }),

/***/ "./styles/main.scss":
/*!**************************!*\
  !*** ./styles/main.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 0:
/*!***********************!*\
  !*** multi ./main.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./main.js */"./main.js");


/***/ })

},[[0,"runtime","vendors"]]]);
//# sourceMappingURL=main.js.map